# Produzindo Músicas Geradas Totalmente por IA Generativa

## 📒 Descrição
Neste projeto demonstro o potencial existente na produção de músicas usando IA Generativas, seja para composição de melodias ou letras, ou apenas para inspiração.

Este foi o Desafio de Projeto do [**Bootcamp Nexa - Fundamentos de IA Generativa e Claude 3**](https://www.dio.me/bootcamp/bootcamp-nexa-fundamentos-de-ia-generativa-e-claude-3), no qual participei.

## 🤖 Tecnologias Utilizadas
- [**ChatGPT**](https://chatgpt.com) - Me ajudou com a ideia, a organização e o planejamento.
- [**Suno**](https://chatgpt.com) - Produziu a música a partir de uma descrição detalhada sobre como a música deveria ser.

## 🧐 Processo de Criação
Primeiramente utilizei o **ChatGPT** para sugestão de ideias, a partir das informações do Bootcamp. Dentre as ideias sugeridas, gostei mais da ideia de produzir uma música usando apenas IA.

Após informar a ideia escolhida, o **ChatGPT** me passou um roteiro de como implementar a ideia.

Após pesquiser por algumas IA's Generativas com a capacidade de produzir músicas, optei por utilizar a **Suno** por já produzir músicas com letras, inclusive em português.

Após alguns testes, solicitei que a IA Generativa produzisse a música com a seguinte descrição:

`Uma música envolvente, no estilo Alternative Rock, que fale sobre como a perfeição de Deus se expressa na natureza.`


## 🚀 Resultados
E aí, você acha que é _Natty_ ou _Fake Natty_?

![Vídeo - A Perfeição na Natureza.mp4](A Perfeição na Natureza.mp4)

![Áudio - A Perfeição na Natureza.mp3](A Perfeição na Natureza.mp3)

Não deixe de curtir a música na plataforma [**Suno**](https://suno.com) usando o link abaixo:

[A Perfeição na Natureza - by Suno](https://suno.com/song/5ec87df0-9b0a-49e1-84bb-582a598c8fe4)
